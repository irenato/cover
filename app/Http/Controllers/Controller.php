<?php

namespace App\Http\Controllers;

use App\Managers\BotManager;
use App\Managers\QuestManager;
use App\Models\Recipient;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Client;
use TelegramBot\Api\Types\Chat;
use TelegramBot\Api\Types\Inline\InlineKeyboardMarkup;
use TelegramBot\Api\Types\Message;
use TelegramBot\Api\Types\ReplyKeyboardMarkup;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private QuestManager $questManager;

    private BotManager $botManager;

    public function __construct(
        QuestManager $questManager,
        BotManager $botManager
    ) {
        $this->questManager = $questManager;
        $this->botManager = $botManager;
    }

    public function index(): void
    {
        $bot = new Client(env('BOT_API_TOKEN'));
        $this->prepareAnswers($bot);
        $bot->command(
            'start',
            function (Message $message) use ($bot): void {
                $recipient = $this->getRecipient($message->getChat());
                $keyboard = new InlineKeyboardMarkup(
                    [
                        [
                            ['text' => 'Ну ок, че', 'callback_data' => 1]
                        ]
                    ]
                );
                $bot->sendMessage(
                    $message->getChat()->getId(),
                    "<b>Привет, {$recipient->username}. Если ты читаешь эти строки - ты таки живой. Не кашляй.</b>",
                    'HTML',
                    false,
                    null,
                    $keyboard
                );
            }
        );
        $bot->run();
    }

    private function prepareAnswers(Client $bot): void
    {
        $actionId = $this->botManager->getCallbackData($bot);
        if (isset($actionId)) {
            $data = $this->questManager->prepareQuestion($actionId, $this->botManager->prepareUsername($bot));
            $bot->sendMessage(
                $this->botManager->getRecipientId($bot),
                $data['message'],
                'HTML',
                false,
                null,
                $data['keyboard']
            );
        }
    }

    private function getRecipient(Chat $chat): Recipient
    {
        return Recipient::firstOrCreate(
            [
                'chat_id' => $chat->getId(),
                'username' => $chat->getFirstName() . ' ' . $chat->getLastName()
            ]
        );
    }
}
