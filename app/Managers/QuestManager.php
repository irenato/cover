<?php


namespace App\Managers;


use App\Models\Quests\Action;
use App\Models\Quests\Quest;
use App\Models\Quests\Question;
use http\Client;
use Illuminate\Database\Eloquent\Model;
use TelegramBot\Api\Types\Chat;

class QuestManager
{
    private DialogManager $dialogManager;

    public function __construct(DialogManager $dialogManager)
    {
        $this->dialogManager = $dialogManager;
    }

    public function prepareQuestion(int $actionId = 1, ?string $username): array
    {
        $action = $this->getAction($actionId);
        return [
            'message' => str_replace('{{username}}', $username, "<b>$action->description</b>"),
            'keyboard' => $this->dialogManager->generateKeyboard($action->options)
        ];
    }

    private function getQuest(int $questId): Model
    {
        return Quest::with('actions')->find($questId);
    }

    private function getAction(int $actionId): Model
    {
        return Action::with('options')->find($actionId);
    }
}
