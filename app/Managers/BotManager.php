<?php


namespace App\Managers;

use phpDocumentor\Reflection\Types\Integer;
use stdClass;
use TelegramBot\Api\Client;

class BotManager
{
    public function getCallbackData(Client $client): ?string
    {
        $rawBody = $this->getRawBody($client);
        if (isset($rawBody->callback_query) && isset($rawBody->callback_query->data)) {
            return $rawBody->callback_query->data;
        }

        return null;
    }

    public function getRecipientId(Client $client): ?int
    {
        $rawBody = $this->getRawBody($client);

        if (isset($rawBody->callback_query) && isset($rawBody->callback_query->from->id)) {
            return $rawBody->callback_query->from->id;
        }

        return null;
    }

    public function prepareUsername(Client $client): ?string
    {
        $rawBody = $this->getRawBody($client);

        if (isset($rawBody->callback_query) && isset($rawBody->callback_query->from->id)) {
            return $rawBody->callback_query->from->first_name;
        }
        return null;
    }

    private function getRawBody(Client $client): StdClass
    {
        return json_decode($client->getRawBody());
    }


}
