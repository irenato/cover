<?php


namespace App\Managers;


use Illuminate\Database\Eloquent\Collection;
use TelegramBot\Api\Types\Chat;
use TelegramBot\Api\Types\Inline\InlineKeyboardMarkup;

class DialogManager
{
    public function generateKeyboard(Collection $optionsData): ?InlineKeyboardMarkup
    {
        $options = $this->generateOptions($optionsData);
        if (!$options) {
            return null;
        }
        return new InlineKeyboardMarkup([$options]);
    }

    private function generateOptions(Collection $optionsData): array
    {
        $buttons = [];
        foreach ($optionsData as $option) {
            $buttons[] = [
                'text' => $option->description,
                'callback_data' => (string)$option->target_id
            ];
        }

        return $buttons;
    }
}
