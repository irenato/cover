<?php

namespace App\Models\Quests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Option extends Model
{
    protected $fillable = [
        'id',
        'description'
    ];

    public function action(): BelongsTo
    {
        return $this->belongsTo(Action::class);
    }
}
