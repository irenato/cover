<?php

namespace App\Models\Quests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Quest extends Model
{
    protected $fillable = [
        'id',
        'title'
    ];

    public function options(): BelongsToMany
    {
        return $this->belongsToMany(Action::class);
    }
}
