<?php

use App\Models\Quests\Option;
use Illuminate\Database\Seeder;

class OptionTableSeeder extends Seeder
{
    public function run(): void
    {
        $options = $this->getData();
        foreach ($options as $optionData) {
            Option::create($optionData);
        }
    }

    private function getData(): array
    {
        return [
            [
                'action_id' => 1,
                'description' => 'В АТБ',
                'target_id' => 2
            ],
            [
                'action_id' => 1,
                'description' => 'В Кулины',
                'target_id' => 3
            ],
            [
                'action_id' => 2,
                'description' => 'Мне только спросить',
                'target_id' => 4
            ],
            [
                'action_id' => 2,
                'description' => 'Стоять ровно',
                'target_id' => 5
            ],
            [
                'action_id' => 3,
                'description' => 'Подмигнуть и насыпать груду мелочи',
                'target_id' => 6
            ],
            [
                'action_id' => 3,
                'description' => 'Не для тебя, старушка, роза росла',
                'target_id' => 7
            ],
            [
                'action_id' => 4,
                'description' => 'Принять бой',
                'target_id' => 9
            ],
            [
                'action_id' => 4,
                'description' => 'Бежать в Кулины',
                'target_id' => 3
            ],
            [
                'action_id' => 5,
                'description' => 'Ждать и стареть',
                'target_id' => 16
            ],
            [
                'action_id' => 5,
                'description' => 'Бросить все и в Кулины',
                'target_id' => 3
            ],
            [
                'action_id' => 6,
                'description' => 'Излучать феромоны на максимуме',
                'target_id' => 10
            ],
            [
                'action_id' => 6,
                'description' => 'Бросив все ретироваться в АТБ',
                'target_id' => 2
            ],
            [
                'action_id' => 7,
                'description' => 'К Волшебнику',
                'target_id' => 11
            ],
            [
                'action_id' => 7,
                'description' => 'В аптеку',
                'target_id' => 32
            ],
            [
                'action_id' => 9,
                'description' => 'Добить старушек',
                'target_id' => 12
            ],
            [
                'action_id' => 9,
                'description' => 'Гордо направиться ко входу',
                'target_id' => 31
            ],
            [
                'action_id' => 10,
                'description' => 'Бежать со всех ног',
                'target_id' => 4
            ],
            [
                'action_id' => 10,
                'description' => 'Проследовать за ней',
                'target_id' => 25
            ],
            [
                'action_id' => 11,
                'description' => 'Согласиться',
                'target_id' => 27
            ],
            [
                'action_id' => 11,
                'description' => 'Вежливо назвать свою цену',
                'target_id' => 27
            ],
            [
                'action_id' => 12,
                'description' => 'Не миловать',
                'target_id' => 13
            ],
            [
                'action_id' => 12,
                'description' => 'Забрать дар',
                'target_id' => 15
            ],
            [
                'action_id' => 13,
                'description' => 'В аптеку',
                'target_id' => 32
            ],
            [
                'action_id' => 13,
                'description' => 'Приложить подорожник',
                'target_id' => 14
            ],
            [
                'action_id' => 14,
                'description' => 'В аптеку',
                'target_id' => 32
            ],
            [
                'action_id' => 15,
                'description' => '1024 извинения',
                'target_id' => 13
            ],
            [
                'action_id' => 15,
                'description' => 'Ну и (censured) с ней',
                'target_id' => 13
            ],
            [
                'action_id' => 16,
                'description' => 'За ходоков',
                'target_id' => 17
            ],
            [
                'action_id' => 16,
                'description' => 'За охрану',
                'target_id' => 18
            ],
            [
                'action_id' => 17,
                'description' => 'Позовите администатора',
                'target_id' => 21
            ],
            [
                'action_id' => 17,
                'description' => 'sudo su',
                'target_id' => 19
            ],
            [
                'action_id' => 18,
                'description' => 'Позвать администратора',
                'target_id' => 21
            ],
            [
                'action_id' => 18,
                'description' => 'sudo su',
                'target_id' => 19
            ],
            [
                'action_id' => 19,
                'description' => 'Разрулить ситуацию',
                'target_id' => 20
            ],
            [
                'action_id' => 19,
                'description' => 'Положить болт',
                'target_id' => 24
            ],
            [
                'action_id' => 21,
                'description' => 'Нарисовать защитный круг',
                'target_id' => 22
            ],
            [
                'action_id' => 21,
                'description' => 'Принять свою судьбу',
                'target_id' => 23
            ],
            [
                'action_id' => 22,
                'description' => 'Принять свою судьбу',
                'target_id' => 23
            ],
            [
                'action_id' => 23,
                'description' => 'Приложить подорожник',
                'target_id' => 14
            ],
            [
                'action_id' => 23,
                'description' => 'В аптеку',
                'target_id' => 32
            ],
            [
                'action_id' => 24,
                'description' => 'Принять свою судьбу',
                'target_id' => 23
            ],
            [
                'action_id' => 25,
                'description' => 'Валяй',
                'target_id' => 23
            ],
            [
                'action_id' => 25,
                'description' => 'Валяй',
                'target_id' => 23
            ],
            [
                'action_id' => 25,
                'description' => 'Отправиться в путь',
                'target_id' => 11
            ],
            [
                'action_id' => 27,
                'description' => 'Согласиться',
                'target_id' => 28
            ],
            [
                'action_id' => 28,
                'description' => 'Сучий пес',
                'target_id' => 29
            ],
            [
                'action_id' => 28,
                'description' => 'Змей',
                'target_id' => 29
            ],
            [
                'action_id' => 28,
                'description' => 'Кнопка',
                'target_id' => 30
            ],
            [
                'action_id' => 29,
                'description' => 'Текст по-дебильному написан',
                'target_id' => 28
            ],
            [
                'action_id' => 30,
                'description' => 'В аптеку',
                'target_id' => 32
            ],
            [
                'action_id' => 31,
                'description' => 'За ходоков',
                'target_id' => 17
            ],
            [
                'action_id' => 31,
                'description' => 'За охрану',
                'target_id' => 18
            ],
        ];
    }
}
