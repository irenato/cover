<?php

use App\Models\Quests\Quest;
use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    public function run(): void
    {
        $quests = $this->getData();
        foreach ($quests as $questData){
            Quest::create($questData);
        }
    }

    private function getData(): array
    {
        return [
            [
                'id' => 1,
                'title' => 'Доверять выживанию',
            ]
        ];
    }
}
