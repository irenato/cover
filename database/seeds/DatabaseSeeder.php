<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
         $this->call(QuestionTableSeeder::class);
         $this->call(ActionTableSeeder::class);
         $this->call(OptionTableSeeder::class);
    }
}
